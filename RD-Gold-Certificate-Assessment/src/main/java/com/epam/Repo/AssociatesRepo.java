package com.epam.Repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.epam.model.Associates;

public interface AssociatesRepo  extends JpaRepository<Associates, Integer>{
	List<Associates> findByGender(String gender);	
}
