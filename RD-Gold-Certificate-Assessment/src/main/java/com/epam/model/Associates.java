package com.epam.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name="associates")
public class Associates {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "batch_id", nullable = false, unique = true)
	private int id;
	@Column(name = "name", nullable = false)
	private String name;
	@Column(name="eamil",nullable=false,unique=true)
	private String email;

	@Column(name="gender",nullable=false)
	private String gender;
	@Column(name="values",nullable=false)
	private String values;
	
	@ManyToOne
	@JoinColumn(name="batch_id",referencedColumnName="batch_id")
	Batches batch;

}
