package com.epam.controller;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.epam.dto.AssociatesDto;
import com.epam.service.AssociatesService;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequiredArgsConstructor
@RequestMapping("/rd/associates")
@Slf4j
public class AssociatesController {

	private final AssociatesService associatesService;
	
	@PostMapping
	public ResponseEntity<AssociatesDto> create (@RequestBody @Valid AssociatesDto associatesDto){
		log.info("Got post request to create Assosiate from user  with associateDto : {}",associatesDto);
		return ResponseEntity.status(201).body((associatesService.add(associatesDto)));
	}
	
	@PutMapping
	public ResponseEntity<AssociatesDto> update(@RequestBody @Valid AssociatesDto associatesDto)
	{
		log.info("got Put request to update Associate value form user with associateDto : {}",associatesDto);
		return ResponseEntity.ok(associatesService.update(associatesDto));
	}
	
	@GetMapping("/{gender}")
	public ResponseEntity<List<AssociatesDto>> getByGender (@PathVariable String gender)
	{
		log.info("got get request to get by gender from user with gender : {}",gender);
		return ResponseEntity.ok(associatesService.getByGender(gender));
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<String> delete (@PathVariable int id)
	{
		log.info("got delete request to delete associate by id : {}",id);
		associatesService.delete(id);
		return ResponseEntity.noContent().build();
	}
	
}