
package com.epam.exception;

public class AssociateException extends RuntimeException{
	/**
	 * 
	 */
	private static final long serialVersionUID = 7900191387082048703L;

	public AssociateException(String message) {
		super(message);
	}

}
