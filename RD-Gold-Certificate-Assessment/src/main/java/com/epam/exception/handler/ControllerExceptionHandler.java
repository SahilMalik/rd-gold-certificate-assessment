package com.epam.exception.handler;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import com.epam.dto.ExceptionResponse;
import com.epam.exception.AssociateException;

import lombok.extern.slf4j.Slf4j;

@RestControllerAdvice
@Slf4j
public class ControllerExceptionHandler {

	@ExceptionHandler(MethodArgumentNotValidException.class)
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	public ExceptionResponse handleMethodArgumentNotValidException(MethodArgumentNotValidException ex, WebRequest req) {
		List<String> errors = new ArrayList<>();
		ex.getAllErrors().forEach(err -> errors.add(err.getDefaultMessage()));
		log.warn("Encountered MethodArgumentNotValidException:{}", ExceptionUtils.getStackTrace(ex));
		return new ExceptionResponse(new Date().toString(), HttpStatus.BAD_REQUEST.name(), errors.toString(),
				req.getDescription(false));
	}

	@ExceptionHandler(MethodArgumentTypeMismatchException.class)
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	public ExceptionResponse handleMethodArgumentTypeMismatchException(MethodArgumentTypeMismatchException ex,
			WebRequest req) {

		ExceptionResponse exceptionResponse = new ExceptionResponse(new Date().toString(),
				HttpStatus.BAD_REQUEST.name(), ex.getMessage(), req.getDescription(false));
		log.warn("Encountered MethodArgumentTypeMismatchException:{}", ExceptionUtils.getStackTrace(ex));
		return exceptionResponse;
	}

	@ExceptionHandler(AssociateException.class)
	public ExceptionResponse handleAssociateException(AssociateException ex, WebRequest req) {
		ExceptionResponse exceptionResponse = new ExceptionResponse(new Date().toString(),
				HttpStatus.BAD_REQUEST.name(), ex.getMessage(), req.getDescription(false));
		log.warn("Encountered AssociateException:{}", ExceptionUtils.getStackTrace(ex));
		return exceptionResponse;
	}

	@ExceptionHandler(DataIntegrityViolationException.class)
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	public ExceptionResponse handleDataIntegrityViolationException(DataIntegrityViolationException ex, WebRequest req) {
		ExceptionResponse exceptionResponse = new ExceptionResponse(new Date().toString(),
				HttpStatus.BAD_REQUEST.name(), ex.getMessage(), req.getDescription(false));
		log.warn("Encountered DataIntegrityViolationException:{}", ExceptionUtils.getStackTrace(ex));
		return exceptionResponse;
	}

	@ExceptionHandler(RuntimeException.class)
	@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
	public ExceptionResponse handleRuntimeException(RuntimeException ex, WebRequest req) {
		ExceptionResponse exceptionResponse = new ExceptionResponse(new Date().toString(),
				HttpStatus.INTERNAL_SERVER_ERROR.name(), "server Error Contact Administrator",
				req.getDescription(false));
		log.warn("Encountered RuntimeException:{}", ExceptionUtils.getStackTrace(ex));
		return exceptionResponse;
	}

}
