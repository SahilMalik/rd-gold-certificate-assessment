package com.epam.dto;

import java.time.LocalDate;
import org.springframework.format.annotation.DateTimeFormat;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;

@Data
public class BatchesDto {
	int id;
	@NotBlank(message = "name cannot be blank")
	String name;
	@NotBlank(message = "practice cannot be blank")
	String practice;
	@NotBlank(message = "start date cannot be blank")
	@DateTimeFormat
	LocalDate startDate;
	@NotBlank(message = "end date cannot be blank")
	@DateTimeFormat
	LocalDate endDate;
}
