package com.epam.service;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import com.epam.Repo.AssociatesRepo;
import com.epam.dto.AssociatesDto;
import com.epam.dto.BatchesDto;
import com.epam.exception.AssociateException;
import com.epam.model.Associates;
import com.epam.model.Batches;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@RequiredArgsConstructor
public class AssociatesServiceImpl implements AssociatesService {
	private final AssociatesRepo associateRepository;
	private final ModelMapper mapper;
	private static final List<String> GENDERS=List.of("M","F");

	@Override
	public AssociatesDto add(AssociatesDto associatesDto) {
		log.info("In service add method request recieved from controller with associateDto : {}",associatesDto);
		String gender = associatesDto.getGender();
		if (!GENDERS.contains(gender)) {
			throw new AssociateException("Enter a valid gender M/F");
		}
		Associates associates = mapper.map(associatesDto, Associates.class);
		associates.setBatch(mapper.map(associatesDto.getBatchesDto(), Batches.class));
		associates=associateRepository.save(associates);
		associatesDto=mapper.map(associates,AssociatesDto.class);
		associatesDto.setBatchesDto(mapper.map(associates.getBatch(), BatchesDto.class));
		log.info("In service add method leaving with assosiateDto : {}",associatesDto);
		return associatesDto;
	}

	@Override
	@Transactional
	public AssociatesDto update(AssociatesDto associatesDto) {
		log.info("In service update method request recieved from controller with associateDto : {}",associatesDto);
		String gender = associatesDto.getGender();
		if (!GENDERS.contains(gender)) {
			throw new AssociateException("Enter a valid gender M/F");
		}
		return associateRepository.findById(associatesDto.getId()).map(a -> {
			a.setBatch(mapper.map(associatesDto.getBatchesDto(),Batches.class));
			//a.setCollege(associatesDto.getCollege());
			a.setEmail(associatesDto.getEmail());
			a.setGender(associatesDto.getGender());
			a.setName(associatesDto.getName());
			//a.setStatus(associatesDto.getStatus());
			log.info("In service update method leaving with assosiateDto : {}",associatesDto);
			return associatesDto;

		}).orElseThrow(() -> new AssociateException("Assosiate with given id is not present check again"));
	}

	@Override
	public void delete(int id) {

		log.info("In service delete method gat with associate id: {}",id);
		associateRepository.deleteById(id);
	}

	@Override
	public List<AssociatesDto> getByGender(String gender) {

		log.info("In service getByGender method with gender : {}",gender);
		return associateRepository.findByGender(gender).stream()
				.map(a -> 
				{
				AssociatesDto associatesDto= mapper.map(a, AssociatesDto.class);
				associatesDto.setBatchesDto(mapper.map(a.getBatch(), BatchesDto.class));

				return associatesDto;
				})
				.toList();
	}
}

