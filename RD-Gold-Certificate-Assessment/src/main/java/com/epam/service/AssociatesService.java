package com.epam.service;

import java.util.List;

import com.epam.dto.AssociatesDto;

public interface AssociatesService {
public AssociatesDto add(AssociatesDto associatesDto);
public AssociatesDto update(AssociatesDto associatesDto);
public void delete(int id);
public List<AssociatesDto> getByGender(String gender);
}
